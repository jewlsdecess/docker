# [docker](https://hub.docker.com/_/docker/)

Docker in Docker!

Unofficial demo and howto

## Read first
* [*Using Docker-in-Docker for your CI or testing environment? Think twice*](https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/)
  [Jérôme Petazzoni](https://github.com/jpetazzo)

## Official documentation
* [*Get Started, Part 1: Orientation and setup*](https://docs.docker.com/get-started/)
* [*Docker overview*](https://docs.docker.com/engine/docker-overview/)
* [*Best practices for writing Dockerfiles*](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
* [*Docker run reference*](https://docs.docker.com/engine/reference/run/#specify-an-init-process)
* [*Dockerfile reference*](https://docs.docker.com/engine/reference/builder/#cmd)

## GitLab documentation
* [*Building Docker images with GitLab CI/CD*](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)
  * [*Passing Docker Image Between Build and Test Stage in GitLab Runner*](https://forum.gitlab.com/t/passing-docker-image-between-build-and-test-stage-in-gitlab-runner/2444)

## Unofficial documentation
* [*Cannot connect to the Docker daemon. Is the docker daemon running on this host?*](https://forums.docker.com/t/cannot-connect-to-the-docker-daemon-is-the-docker-daemon-running-on-this-host/8925)
* [krallin/tini](https://github.com/krallin/tini) A tiny but valid `init` for containers
* [*Docker RUN vs CMD vs ENTRYPOINT*](https://goinbigdata.com/docker-run-vs-cmd-vs-entrypoint/)
  2016-04 Yury Pitsishin

## Some other projects building Docker images
* https://gitlab.com/javascript-packages-demo/foundation-cli